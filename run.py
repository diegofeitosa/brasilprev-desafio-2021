import csv
from dotenv import dotenv_values
from src.atores import Jogador, Propriedade
from src.jogadas import Iniciativa, Turno

print("BrasilPrev - Desafio - 2021")
print("")

config = dotenv_values(".env")
logs = []

for partida in range(int(config['PARTIDAS'])):

    jogadores = []
    propriedades = []

    with open('data/jogadores.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for index, row in enumerate(csv_reader):
            if index > 0:
                jogador = Jogador(row[0], row[1], row[2])
                jogadores.append(jogador)

    with open('data/propriedades.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for index, row in enumerate(csv_reader):
            if index > 0:
                propriedade = Propriedade(row[0], row[1], row[2])
                propriedades.append(propriedade)

    logs_partida = {
        'numero': partida+1,
        'timeout': False,
        'turnos': None,
        'campeao': None
    }

    print()
    print("# Partida " + str(partida+1))

    iniciativa = Iniciativa(jogadores)
    jogadores = iniciativa.ordenar(jogadores)
    turno = Turno(propriedades)
    maximo_rodadas = int(config['RODADAS_MAX'])
    qtde_turno = 0

    for rodada in range(maximo_rodadas):
        print()
        print("### Rodada Nº " + str(rodada+1) + " ###")

        jogadores_em_jogo = list(filter(lambda jogador: jogador.saldo > 0, jogadores))

        if len(jogadores_em_jogo) < 2:
            print("Vencedor é o jogador " + jogadores_em_jogo[0].nome)
            logs_partida['turnos'] = qtde_turno
            logs_partida['campeao'] = jogadores_em_jogo[0]
            break

        for jogador in jogadores_em_jogo:
            print()
            print("Turno do " + jogador.nome + " - Saldo de $" + str(round(jogador.saldo, 2)))
            turno.localizacao(jogador)
            turno.mover(jogador)
            turno.localizacao(jogador)
            turno.acao_na_propriedade(jogador)
            if jogador.saldo <= 0:
                turno.falencia(jogador)
            qtde_turno = qtde_turno + 1

        if rodada+1 == maximo_rodadas:
            finalistas = sorted(jogadores_em_jogo, key=lambda d: d.saldo, reverse=True)
            print()
            print("Limite de Rodadas alcançado, o vencedor é o jogador " + finalistas[0].nome)
            for index, jogador in enumerate(finalistas):
                print("  " + str(index+1) + "º: " + jogador.nome + " com $" + str(round(jogador.saldo, 2)))
            logs_partida['timeout'] = True
            logs_partida['turnos'] = qtde_turno
            logs_partida['campeao'] = jogadores_em_jogo[0]
            break

    logs.append(logs_partida)
    print("### Fim da partida " + str(partida+1) + " ###")

print()
print("# Relatório das partidas")

qtde_logs = len(logs)
por_timeout = list(filter(lambda log: log['timeout'] == True, logs))
media_turnos = sum(log['turnos'] for log in logs) / qtde_logs
vitorias_por_comportamento = []


for jogador in jogadores:
    vitorias = list(filter(lambda d: d['campeao'].nome == jogador.nome, logs))
    qtde_vitorias = len(vitorias)

    vitorias_por_comportamento.append({
        'jogador': jogador,
        'quantidade': qtde_vitorias,
        'percent': (qtde_vitorias * 100) / qtde_logs
    })

maior_vitorias = max(vitorias_por_comportamento, key=lambda x:int(x['quantidade']))

print("Quantas partidas terminaram por time out?")
print("  " + str(len(por_timeout)) + " partidas.")
print("Quantos turnos em média demora uma partida?")
print("  " + str(round(media_turnos, 2)) + " turnos.")
print("Qual a porcentagem de vitórias por comportamento dos jogadores?")
for vitoria in vitorias_por_comportamento:
    print(" - " + vitoria['jogador'].nome + " venceu " + str(round(vitoria['percent'], 2)) + "%")
print("Qual o comportamento que mais vence?")
print("  " + maior_vitorias['jogador'].nome + " venceu " + str(maior_vitorias['quantidade']) + " vezes de " + config['PARTIDAS'])
if maior_vitorias['jogador'].aluguel_min > 0:
    print("  Apenas comprava imóveis cujo valor de aluguel era superior a " + str(maior_vitorias['jogador'].aluguel_min) + ".")
else:
    print("  Comprava imóveis com qualquer valor de aluguel.")
if maior_vitorias['jogador'].reserva_min > 0:
    print("  Apenas comprava a propriedade se ao final da compra ainda tivesse pelo menos $" + str(maior_vitorias['jogador'].reserva_min) + " de saldo.")
else:
    print("  Comprava qualquer propriedade que tivesse dinheiro para comprar.")
