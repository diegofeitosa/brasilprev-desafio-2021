# BrasilPrev - Desafio - 2021

Desafio proposto para a vaga de emprego na Brasil Prev no ano de 2021 para a vaga em Python

Desafio pode ser visto [clicando aqui](DESAFIO_PYTHONDX%20(3)%20(1).pdf)

## Requisitos
- [Python 3](https://www.python.org/)
- [Python-dotenv](https://pypi.org/project/python-dotenv/)

## Configuração

O projeto utiliza um arquivo não versionável chamado '.env', onde fica guardado as configurações do projeto. Antes de rodar o projeto é necessário criar esse arquivo a partir do arquivo ".env.example". No arquivo '.env' é possível configurar a quantidade de partidas e máximo de rodadas por partida ou mesmo o saldo inicial dos jogadores, por exemplo.

Para que o Python entenda o arquivo '.env' é necessário que a lib python-dotenv esteja instalado no ambiente.

Existe uma pasta chamada data onde ficam armazenados arquivos '.csv' que serão utilizados para popular a aplicação com informações de jogadores e propriedades. Caso deseja adicionar, remover ou alterar as informações dos jogadores ou propriedades, basta acessar o arquivo respectivo.

## Rodar a aplicação

Para rodar a aplicação, basta executar o arquivo 'run.py' com python 3.
