import random
import uuid
from dotenv import dotenv_values

class Jogador:
    def __init__(self, nome=None, reserva=None, aluguel=None):
        config = dotenv_values(".env")
        if reserva.strip() == "RAND":
            reserva = random.uniform(0, int(config['JOGADORES_SALDO']))
        if aluguel.strip() == "RAND":
            custo_max = random.uniform(0, int(config['PROPRIEDADES_VALOR_MAX']))
            aluguel = custo_max * (int(config['PROPRIEDADES_ALUGUEL_PORCENTAGEM']) / 100)
        self.id = uuid.uuid4()
        self.nome = nome
        self.saldo = float(config['JOGADORES_SALDO'])
        self.aluguel_min = round(float(aluguel), 2)
        self.reserva_min = round(float(reserva), 2)
        self.posicao = 0

    def __eq__(self, other):
        return self.id == other.id


class Propriedade:
    def __init__(self, nome=None, venda=None, aluguel=None):
        config = dotenv_values(".env")
        if venda.strip() == "RAND":
            valor_minimo = int(config['PROPRIEDADES_VALOR_MIN'])
            valor_maximo = int(config['PROPRIEDADES_VALOR_MAX'])
            venda = random.uniform(valor_minimo, valor_maximo)
        if aluguel.strip() == "RAND":
            valor_minimo = int(config['PROPRIEDADES_VALOR_MIN'])
            valor_maximo = int(config['PROPRIEDADES_VALOR_MAX'])
            porcentagem = (int(config['PROPRIEDADES_ALUGUEL_PORCENTAGEM']) / 100)
            aluguel = random.uniform(valor_minimo, valor_maximo) * porcentagem
        self.id = uuid.uuid4()
        self.nome = nome
        self.venda = round(float(venda), 2)
        self.aluguel = round(float(aluguel), 2)
        self.proprietario = None
