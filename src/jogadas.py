import random

class Iniciativa:

    def __init__(self, jogadores=[]):
        self.jogadores = self.rolar(jogadores)
        self.listar_ordem()

    def listar_ordem(self):
        print()
        print("Ordem dos Jogadores na partida:")
        for index, jogador in enumerate(sorted(self.jogadores, key=lambda d: d.ordem)):
            jogador.ordem = index+1
            print(str(index+1) + "º - " + jogador.nome)

    def ordenar(self, jogadores):
        return sorted(jogadores, key=lambda d: d.ordem)

    def ordenar_por_iniciativa(self, jogadores):
        jogadores = sorted(jogadores, key=lambda d: d.iniciativa)
        for index, jogador in enumerate(jogadores):
            jogador.ordem = index+1
        return sorted(jogadores, key=lambda d: d.ordem)

    def rolar(self, jogadores):
        print("Rolar a iniciativa dos jogadores...")
        for jogador in jogadores:
            iniciativa = random.randint(1, 6)
            jogador.iniciativa = iniciativa
            print("  " + jogador.nome + " rolou " + str(iniciativa))
        jogadores = self.ordenar_por_iniciativa(jogadores)
        self.verificar_empates(jogadores)
        return jogadores

    def verificar_empates(self, jogadores):
        empatados = []
        verificados = []
        for jogador_a in jogadores:
            empataram = []
            for jogador_b in jogadores:
                mesmo_jogador = jogador_a == jogador_b
                jogador_verificado = jogador_b in verificados
                if mesmo_jogador or jogador_verificado:
                    continue
                if jogador_a.iniciativa == jogador_b.iniciativa:
                    empataram.append(jogador_b)
                    verificados.append(jogador_b)
            if empataram:
                empataram.append(jogador_a)
                empatados.append(empataram)
            verificados.append(jogador_a)

        for grupo in empatados:
            posicao = grupo[0].ordem
            grupo = self.rolar(grupo)


class Turno:

    def __init__(self, propriedades):
        self.propriedades = propriedades

    def localizacao(self, jogador):
        propriedade = self.propriedades[jogador.posicao-1]
        if jogador.posicao == 0:
            print("  Está na posição inicial do tabuleiro.")
        else:
            print("  Está na posição " + str(jogador.posicao) + " do tabuleiro.")
            print("  A propriedade é a " + propriedade.nome)
            if propriedade.proprietario:
                print("    Pertence a " + propriedade.proprietario.nome)
                print("   Taxa do aluguel é de $" + str(round(propriedade.aluguel, 2)))
            else:
                print("    Está a venda por $" + str(round(propriedade.venda, 2)))
                print("    Valor do aluguel é de $" + str(round(propriedade.aluguel, 2)))

    def mover(self, jogador):
        print("  Rolar ação de movimento")
        andar = random.randint(1, 6)
        print("    Rolou um " + str(andar) + " no dado!")
        nova_posicao = jogador.posicao + andar
        if nova_posicao > 20:
            print("  Completou uma volta no tabuleiro. Irá receber $100,00.")
            nova_posicao -= 21
            jogador.saldo += 100.0
        jogador.posicao = nova_posicao

    def acao_na_propriedade(self, jogador):
        propriedade = self.propriedades[jogador.posicao-1]
        if not propriedade.proprietario:
            if jogador.saldo >= propriedade.venda:
                if self.comportamento_compra(jogador, propriedade):
                    jogador.saldo -= propriedade.venda
                    propriedade.proprietario = jogador
                    print("  Comprou a propriedade " + propriedade.nome)
            else:
                print("  Valor da propriedade é maior que o saldo.")
        else:
            if propriedade.proprietario == jogador:
                print("  Já é dono da propriedade.")
            else:
                print("  O dono da propriedade é " + propriedade.proprietario.nome)
                print("  Será pago uma taxa de aluguel de $" + str(round(propriedade.aluguel, 2)))
                if propriedade.aluguel > jogador.saldo:
                    propriedade.proprietario.saldo += jogador.saldo
                else:
                    propriedade.proprietario.saldo += propriedade.aluguel
                jogador.saldo -= propriedade.aluguel
        print("  Saldo atual: $" + str(round(jogador.saldo, 2)))

    def comportamento_compra(self, jogador, propriedade):
        reserva = jogador.saldo - propriedade.venda
        requisito_aluguel = propriedade.aluguel > jogador.aluguel_min
        requisito_saldo = reserva >= jogador.reserva_min

        if not requisito_aluguel:
            print("  O valor do aluguel é mais baixo que o esperado pelo jogador.")

        if not requisito_saldo:
            print("  O valor de saldo ficaria abaixo que o esperado pelo jogador.")

        return requisito_aluguel and requisito_saldo

    def falencia(self, jogador):        
        hipotecadas = list(filter(lambda propriedade: propriedade.proprietario and propriedade.proprietario == jogador, self.propriedades))
        if hipotecadas:
            print("  Jogador faliu. As seguintes propriedades estão disponíveis para a compra:")
            for propriedade in hipotecadas:
                propriedade.proprietario = None
                print("  - " + propriedade.nome)

        else:
            print("  Jogador faliu")
